// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

(async function () {
  window.addEventListener("load", function () {
    const requiredScroll = 20;
    let buttonGoToTop = document.getElementById("btn-go-to-top");

    window.onscroll = function () {
      if (document.documentElement.scrollTop > requiredScroll) {
        buttonGoToTop.style.display = "block";
      } else {
        buttonGoToTop.style.display = "none";
      }
    };

    buttonGoToTop.addEventListener("click", function () {
      document.body.scrollTop = 0;
      document.documentElement.scrollTop = 0;
    });
  })
})();
