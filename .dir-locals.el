;; -*- no-byte-compile: t -*-

;; Directory Local Variables
;; For more information see (info "(emacs) Directory Variables")

((nil . ((projectile-project-type . make)
         (projectile-compilation-dir . "./Source/v1/")
         (eval . (add-hook 'before-save-hook 'whitespace-cleanup))))
 (find-file . ((indent-tabs-mode . nil)
               (require-final-newline . t)
               (show-trailing-whitespace . t)))
 (makefile-mode . ((indent-tabs-mode . t)
                   (tab-width . 4)))
 (yaml-mode . ((tab-width . 2))))
