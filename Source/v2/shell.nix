{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  nativeBuildInputs = with pkgs.buildPackages; [
    # Native project-specific packages
    gnumake
    nodejs
    racket

    # Fundamental packages
    git
    glibcLocales
  ];

  shellHook = ''
    ./nix/shell_hook.sh
  '';
}
