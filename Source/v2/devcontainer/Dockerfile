# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

FROM mcr.microsoft.com/devcontainers/base:bookworm

# Switch to "root" account for system-wide software installation.
USER root

# Install needed software.
RUN export DEBIAN_FRONTEND=noninteractive \
    && apt-get update \
    && apt-get install --no-install-recommends -y \
    npm python3-pygments racket

# Configurations for the "vscode" user account.
RUN chsh -s /bin/zsh vscode

# Switch back to default "vscode" account.
USER vscode

RUN racket -l raco -- pkg install --auto --no-docs --user \
    req \
    "https://gitlab.com/xgqt/xgqt-racket-app-frog.git?path=Source%2Fv1%2Ffrog"

EXPOSE 5109
