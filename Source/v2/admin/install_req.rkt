#!/usr/bin/env racket

;; This Source Code Form is subject to the terms of the Mozilla Public
;; License, v. 2.0. If a copy of the MPL was not distributed with this
;; file, You can obtain one at http://mozilla.org/MPL/2.0/.

#lang racket/base

(require racket/path)
(require racket/system)

(define (main)
  (define script-path
    (path->complete-path (find-system-path 'run-file)))
  (define script-directory
    (path-only script-path))
  (define source-path
    (simplify-path (build-path script-directory "..")))

  (current-directory source-path)

  (define racket-exe
    (find-executable-path "racket"))

  (let ()
    (define command-args
      '("-l" "raco" "pkg" "install" "--auto" "--no-docs" "--skip-installed" "req-lib"))
    (define command-success?
      (apply system* racket-exe command-args))

    (unless command-success?
      (displayln "; Errors occurred, exiting with failure")
      (exit 1))))

(module+ main
  (main))
