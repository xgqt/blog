# -*- make -*-

FROG            := $(SH) $(PWD)/frog.sh

.PHONY: all
all:

.PHONY: clean
clean:
	$(FROG) --very-verbose --clean

.PHONY: build
build:
	$(FROG) --very-verbose --build
