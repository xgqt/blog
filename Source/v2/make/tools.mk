# -*- make -*-

MAKE            ?= make
SH              ?= sh

GIT             := git
NPM             := npm
PYTHON          := python3
RACKET          := racket
