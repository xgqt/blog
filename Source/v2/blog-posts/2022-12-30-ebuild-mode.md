    Title: Ebuild-mode
    Date: 2022-12-30T00:00:00
    Tags: gentoo, portage, ebuild, emacs, tutorial, pkgcheck

# Portage

Configure the following for Portage.

``` conf
dev-util/pkgcheck emacs
```

## Emerge

Emerge the following packages:

-   `app-emacs/company-ebuild`
-   `dev-util/pkgcheck`

Company-Ebuild should pull in `app-emacs/ebuild-mode`, if
that does not happen, then report a bug ;-D

# Standard

Add the following to your user\'s Emacs initialization file. The
initialization file is either `~/.emacs.d/init.el` or
`~/.config/emacs/init.el` for newer versions of GNU Emacs.

``` elisp
(require 'ebuild-mode)
(require 'company-ebuild)
(require 'flycheck)
(require 'flycheck-pkgcheck)

(add-hook 'ebuild-mode-hook 'company-ebuild-setup)
(add-hook 'ebuild-mode-hook 'flycheck-mode)
(add-hook 'ebuild-mode-hook 'flycheck-pkgcheck-setup)
```

# Use-Package

We can also configure our environment using a `use-package`
macro that simplifies the setup a little bit.

To use the below configuration the `app-emacs/use-package`
package will have to be installed.

``` elisp
(require 'use-package)

(use-package ebuild-mode
  :defer t
  :mode "\\.\\(ebuild\\|eclass\\)\\'"
  :hook
  ((ebuild-mode . company-ebuild-setup)
   (ebuild-mode . flycheck-mode)
   (ebuild-mode . flycheck-pkgcheck-setup)))
```

The `:defer t` and `:mode "..."` enable deferred
loading which theoretically speeds up GNU Emacs initialization time at
the cost of running the whole `use-package` block of
`ebuild-mode` configuration when the `:mode`
condition is met.
