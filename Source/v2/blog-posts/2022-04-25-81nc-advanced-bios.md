    Title: Lenovo 81nc advanced bios options
    Date: 2022-04-25T00:00:00
    Tags: hardware, laptop, tutorial

# Accessing advanced BIOS menu

## Preparation

First using a needle or a toothpick click a button on the right side of
the laptop (indicated by a bent arrow), this will start the machine up
in BIOS selection menu where you choose the BIOS setup option.

Then, in the BIOS menu disable option to check laptop charge level while
it is shut down. If not disabled it will interfere with key combination
that has to be pressed while laptop is shut down. This can be turned
back after the advanced BIOS menu is enabled.

After that \"Exit saving changes\" and shutdown.

## Keys

I thought it was a joke at first but it really is true that a special
combination of keys has to be pressed depending on the laptop model
(while it is shut down).

For my laptop it is as follows:

``` fundamental
F4 4 r f v
F5 5 t g b
F6 6 y h n
```

# References

-   Lenovo product page:
    <https://pcsupport.lenovo.com/us/en/products/laptops-and-netbooks/ideapad-s-series-netbooks/s340-15api/81nc>
-   <https://www.reddit.com/r/Fedora/comments/r2m6my/comment/hm9dddn/?utm_source=share&utm_medium=web2x&context=3>
