    Title: Using gzexe for shipping Racket executables
    Date: 2023-09-28T20:18:12
    Tags: lisp, racket, scheme, tutorial

Racket executables made by `raco exe` are known to be quite large.
One of tools that can be used to help reduce the size of produced binaries is
the `gzexe` program.

`gzeze` is a tool that can compress a executable binary.
It can be acquired by installing `gzip` on most Linux distributions
(included in the `app-arch/gzip` package on Gentoo).

## Creating a hello-world executable with Racket

Write following contents to `hello-world.rkt` file:

```racket
#lang racket/base

(define (main)
  (displayln "It REPLs. Ship it!"))

(module+ main
  (main))
```

To make a binary run:

```shell
raco exe --orig-exe -v -o hello-world hello-world.rkt
```

The file `hello-world` will be produced.

This is what `file hello-world` says about it:

```text
hello-world: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV),
dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2,
for GNU/Linux 3.2.0, stripped
```

This "small" executable weights **46 MB**!

In comparison `busybox` weights around *2 MB*.

## Compressing with gzexe

Keep in mind that `gzeze` will overwrite the compressed file and create
a backup with appended "~".

```shell
gzexe hello-world
```

And this gives us only **8,5 MB**. Nice!

In comparison `bazel`, which is a single-binary build system written in JAVA,
executable takes *33 MB* on my Gentoo machine. I tried compressing it with
`gzexe` and it reduces it only by *10%*, to around *29 MB*.

`gzexe `is not a silver bullet but with Racket exes it works very nicely.
