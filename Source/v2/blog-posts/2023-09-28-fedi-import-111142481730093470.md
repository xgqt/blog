    Title: Using light theme in a sunny day feels a lot better than usi...
    Date: 2023-09-28T11:14:44
    Tags: fediverse

Using light theme in a sunny day feels a lot better than using a dark theme.

I use "ef-day" in [#Emacs](https://emacs.ch/tags/Emacs) and
"Solarized Light" in other IDEs and terminals.

<small>
    Imported via Fedimpost from [emacs.ch/@xgqt/111142481730093470](https://emacs.ch/@xgqt/111142481730093470)
</small>
