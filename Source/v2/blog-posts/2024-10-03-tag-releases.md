    Title: Tag the releases
    Date: 2024-10-03T17:43:33
    Tags: development, git

Tag your releases!

There were so many times when I wanted to use some package, it has a version on
repology, but when I open the project's repo there are no tags.

Nowadays it is extremely easy to bump software versions according to
semantic versioning and also to tag them, that it should go without saying that
most OSS and proprietary projects could also follow that workflow.

There are many projects both language-specific and agnostic to manage the
release (and tagging) process.
My favorite agnostic one is [tbump](https://github.com/your-tools/tbump/)
and I highly recommend it.
