    Title: Runing nginx under a local user
    Date: 2023-05-19T01:17:24
    Tags: admin, http, network, nginx, server, tutorial

# Config

First let's prepare a suitable nginx configuration file.

This one is pretty bare but it works well for our case:

```nginx
worker_processes 1;
daemon off;
pid ./nginx/temp/nginx.pid;

error_log /dev/stdout info;

events {
    worker_connections 1024;
}

http {
    client_body_temp_path ./nginx/temp/client 1 2;
    proxy_temp_path ./nginx/temp/proxy;
    fastcgi_temp_path ./nginx/temp/fastcgi;
    uwsgi_temp_path ./nginx/temp/uwsgi;
    scgi_temp_path ./nginx/temp/scgi;

    server {
        listen 127.0.0.1:8080;
        server_name localhost;

        access_log /dev/stdout;
        error_log /dev/stdout info;

        root ./;

        location / {
            autoindex on;
        }
    }
}
```

Server config is set up for serving **all** static files from the current
directory.

# Startup

## Preparation

Based on how you want to store `_temp_path` files it might be necessary to
create (or clean up) additional directories, for example:

```shell
rm -r ./nginx/temp
mkdir -p ./nginx/temp
```

## Run in current directory

```shell
nginx -c ./nginx.conf -p ./
```

BTW, you may want to replace `./` with `"$(pwd)"` and occurrences in the config
with static paths.

# Bonus: other simple servers

Some of no-dependency-except-itself http servers it's good to know about:

## Python http.server

```shell
python3 -m http.server -b 127.0.0.1 8080
```

## Busybox

```shell
busybox httpd -f -p 127.0.0.1:8080 -v
```

You can read more about configuring busybox's httpd on
[OpenWRT docs](https://openwrt.org/docs/guide-user/services/webserver/http.httpd).
