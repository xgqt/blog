    Title: Firefox is still the best browser. Deal with it Google!
    Date: 2023-11-22T00:40:59
    Tags: browser, firefox, linux

Firefox began as the first open source browser to live through the browser
wars, overcoming Microsoft's Internet Explorer and continues to deliver
competition-smashing technology to this day.

## Chromium code

The only advantages of Chromium are that it was adopted by Electron and spread
partially because of a more liberal license and Google's own efforts.

Google will never be able to cope with the worst imaginable code base of
Chromium.

### Chromium is near-impossible to compile

On a 4cores/8threads Ryzen CPU Chromium compiles in ~12h and requires at least
20GB of disk space for build.
At the same time Firefox compiles in ~1.5h and requires ~8GB for disk space.

### Programming language adoption

Additionally Firefox team was able to rewrite a very large portion of Firefox
codebase in Rust which improved the browser's safety.
There were attempts to add rust to Chromium but they all are in more of
a addon-like fashion.

### Porting to UNIXes

Because Chromium is extremely large it's very hard to port and maintain for
Linux and BSD based systems.
There were numerous bugs with Chromium's UI on Linux that cause crashes on
pressing random controls.
I believe Google has no Linux testers beside
the "free software freeloaders" (wink, wink, IBM :P).

## Anti-competition

This days Google has to result to dirty tactics where certain Google-owned
websites would either refuse to work on Firefox or give a fake performance hit
that is entirely caused by malicious JavaScript code.

Several popular FOSS-related sources have covered this news recently,
check them out on the WWW.
