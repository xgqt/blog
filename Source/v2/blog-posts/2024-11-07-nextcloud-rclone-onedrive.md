    Title: Interacting with OneDrive in Nextcloud by using RClone
    Date: 2024-11-07T16:57:37
    Tags: admin, microsoft, nextcloud, onedrive, system, tutorial

## How it works

* There will be a FUSE OneDrive filesystem mounted on the machine Nextcloud
  runs on,
* You configure filesystem-local "External storage" and point to the mountpoint
  of a cloud drive (in this case OneDrive),
* users connected via Nextcloud Client will have a option to sync any chosen
  files from the External storage as if they were Nextcloud-owned files.

## Benefits

There are some benefits from connecting OneDrive to Nextcloud:

* faster cached sync than by normal straight-to OneDrive connection - files
  will be pushed to Nextcloud and then uploaded to Nextcloud,
  this will take less time than uploading straight to OneDrive
  **because of Microsoft OneDrive rate-limiting** the uploads
  (especially in case of larger files),
* clients of Nextcloud do not have to configure any OneDrive connection,
* you will be able to have two-way sync of your OneDrive files
  (currently two-way sync on the RClone side is experimental,
  this will use Nextcloud's two-way sync mechanism).

## Set up RClone

First You will have to set up RClone. Connect to your cloud of choice and then
copy the config to a location that will be readable by a service that mounts
a given cloud drive.

For OneDrive I use `/usr/local/share/rclone/config/rclone.conf`
that is accessible only to the `apache` user.

The config will look something like this:

```conf
[OneDrive]
type = onedrive
region = global
token = *REDACTED*
drive_id = *REDACTED*
drive_type = personal
```

## Mounting OneDrive

I created this helper script (in `/usr/local/bin/rclone-mount.sh`):

```shell
#!/bin/sh

set -e
set -u
trap "exit 128" INT

conf_path="${1}"

local_user="${2}"
local_path="${3}"

cloud_name="${4}"
cloud_path="${5}"

log_dir="/var/log/rclone"
log_file="${log_dir}/rclone-mount-${local_user}-${cloud_name}.log"

mkdir -p "${log_dir}"
touch "${log_file}"

chmod a+r "${log_file}"
chown "${local_user}" "${log_file}"

exec rclone                                         \
     --default-permissions                          \
     --allow-other                                  \
     --verbose                                      \
     --vfs-cache-mode full                          \
     --config "${conf_path}"                        \
     mount                                          \
     "${cloud_name}:${cloud_path}" "${local_path}"  \
     >> "${log_file}" 2>&1
```

Then, I use it in a OpenRC service like this (`/etc/init.d/mount-OneDrive`):

```shell
#!/sbin/openrc-run

conf_path="/usr/local/share/rclone/config/rclone.conf"

cloud_name="OneDrive"
cloud_path="/"

local_user="apache"
local_path="/mnt/${cloud_name}"

command="/usr/local/bin/rclone-mount.sh"
command_args="${conf_path} ${local_user} ${local_path} ${cloud_name} ${cloud_path}"

command_background="false"
command_user="${local_user}:$(id -g -n ${local_user})"
supervisor="supervise-daemon"

depend() {
    need net
}

start_pre() {
    ebegin "Unmounting leftovers from ${local_path} before service start"
    umount "${local_path}"
    eend 0
}

stop_post() {
    ebegin "Unmounting leftovers from ${local_path} after service stop"
    umount "${local_path}"
    eend 0
}
```

### Enabling the RClone service

Set up directories and correct permissions:

```shell
mkdir -p /usr/local/share/rclone/config
chown -R apache:apache /usr/local/share/rclone/config

mkdir -p /var/log/rclone
chown -R apache:apache /var/log/rclone
```

Do not forget to make the mount service script executable:

```shell
chmod +x /etc/init.d/mount-OneDrive
```

Enable and start this service on OpenRC:

```shell
rc-update add mount-OneDrive default
rc-service mount-OneDrive start
```

### Drive permissions

RClone mounts cloud drives by using FUSE.
To have the RClone option `--allow-other` available in order to allow `root`
to access the drive you will have to modify the FUSE config file
(`/etc/fuse.conf`) - add `user_allow_other`.

## Nextcloud configuration

Download and enable the "External storage" app.
Then, in "Administration" settings add a external storage:

* name: `ExternalStorage_OneDrive`
* type: `Local`
* authentication: `None`
* configuration: `/mnt/OneDrive`
* available for: *YOUR USER*

<img src="/blog/img/nextcloud/nextcloud_admin_external_storage_onedrive.jpg"
    alt="nextcloud admin external storage onedrive"
    width="800">
