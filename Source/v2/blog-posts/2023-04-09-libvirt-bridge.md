    Title: Libvirt with bridge network
    Date: 2023-04-09T22:28:38
    Tags: libvirt, virtualization, vm, kvm, system, tutorial, linux

# User-mode

By default you would probably have something like this,
the user-mode network:

```xml
<interface type="user">
  <mac address="00:00:00:00:00:00"/>
  <model type="virtio"/>
  <address type="pci" domain="0x0000" bus="0x01" slot="0x00" function="0x0"/>
</interface>
```

# Bridge

Bridges can be easily created using the NetworkManager's TUI tool called
`nmtui`.

## Bridge XML configuration for Libvirt

```xml
<interface type="bridge">
  <mac address="00:00:00:00:00:00"/>
  <source bridge="br1"/>
  <target dev="vnet2"/>
  <model type="virtio"/>
  <alias name="net0"/>
  <address type="pci" domain="0x0000" bus="0x06" slot="0x00" function="0x0"/>
</interface>
```

# Sysctl options

Be sure the following options are enabled (`1`):

- `net.ipv4.ip_forward`
- `net.ipv4.conf.all.send_redirects`

and the following options are disabled (`0`):

- `net.bridge.bridge-nf-call-iptables`
