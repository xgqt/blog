    Title: src_snapshot
    Date: 2022-12-30T00:00:00
    Tags: ebuild, gentoo, portage, prototype

# Prototype

Recently while browsing the Alpine git repo I noticed they have a
function called `snapshot`, see:
<https://git.alpinelinux.org/aports/tree/testing/dart/APKBUILD#n45> I am
not 100% sure about how that works but a wild guess is that the
developers can run that function to fetch the sources and maybe later
upload them to the Alpine repo or some sort of (cloud?) storage.

In Portage there exists a `pkg_config` function used to run
miscellaneous configuration for packages. The only major difference
between `src_snapshot` and that would of course be that users
would never run `snapshot`.

## Sandbox

Probably only the `network sandbox` would have to be lifted
out... to fetch the sources of course.

But also a few (at least one?) special directories and variables would
be useful.
