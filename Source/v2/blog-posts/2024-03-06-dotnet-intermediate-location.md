    Title: Change location of intermediate objects in .NET
    Date: 2024-03-06T23:32:17
    Tags: dotnet, programming, software engineering

.NET creates the so-called intermediate objects while building .NET projects,
those are located in the "bin" and "obj" directories.
The default is not very satisfying, primarily because if a program from
a different machine or a container modifies those, then any cached file system
paths that are encoded in the objects will be broken.
But also it is probably mostly a legacy behavior to have them split between
"bin" and "obj" directories.

I prefer for them to say in one - ".cache",
because that's that they are - cache.
With the following configuration objects will be stored inside
the ".cache" directory.
Furthermore, the objects produced by the native machine in
the "native" subdirectory and the ones produced by container software
in "container" subdirectory.

```xml
<PropertyGroup>
  <CachePath>.\.cache\native</CachePath>
  <CachePath Condition="'$(DOTNET_RUNNING_IN_CONTAINER)' == 'true'">.\.cache\container</CachePath>
  <MSBUildProjectExtensionsPath>$(CachePath)\obj\</MSBUildProjectExtensionsPath>
  <BaseIntermediateOutputPath>$(CachePath)\obj\</BaseIntermediateOutputPath>
  <BaseOutputPath>$(CachePath)\bin\</BaseOutputPath>
</PropertyGroup>
```

If anybody want to go hardcore and cache the intermediate objects based on the
[RID](https://learn.microsoft.com/en-us/dotnet/core/rid-catalog) or
[architecture triplet](https://wiki.osdev.org/Target_Triplet), then this can
also be done, for example, by adding environment variables to the path.
