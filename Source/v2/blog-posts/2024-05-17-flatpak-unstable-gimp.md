    Title: Installing unstable GIMP with Flatpak
    Date: 2024-05-17T23:35:17
    Tags: gimp, flatpak, packages, packaging, system

## Add Flathub-Beta

Add the Flathub-Beta remote repository:

```shell
flatpak remote-add --user flathub-beta https://flathub.org/beta-repo/flathub-beta.flatpakrepo
```

## Install GIMP beta

Install `org.gimp.GIMP` form `flathub-beta`:

```shell
flatpak install --assumeyes --user flathub-beta org.gimp.GIMP
```

## Run GIMP

If you have other GIMP versions installed you will have to specify the "beta"
version with `//beta`.

```shell
flatpak run org.gimp.GIMP//beta
```

Otherwise you can just run:

```shell
flatpak run org.gimp.GIMP
```

Also, in the desktop menus (like KRunner) this version of GIMP will have
`(beta)` in its name so there is no chance to miss it.
