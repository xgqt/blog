    Title: Install Bazel ZSH completion
    Date: 2024-08-02T20:20:08
    Tags: bazel, bazelisk, build, buildsystem, development, zsh, shell

```shell
(
    repo="bazelbuild/bazel";
    raw="https://raw.githubusercontent.com/${repo}";
    url="${raw}/master/scripts/zsh_completion/_bazel";
    site_functions="/usr/share/zsh/site-functions";

    wget "${url}" -O ${site_functions}/_bazel
)
```

Afterwards, restart your ZSH shell session.

This also works if you have `bazelisk` installed but `bazel` has to be
symlinked to the `bazelisk` executable path.
