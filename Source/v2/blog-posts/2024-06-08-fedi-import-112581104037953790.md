    Title: The dilemma between and is this: The most important value of...
    Date: 2024-06-08T12:55:18
    Tags: fediverse, emacs.ch

The dilemma between [#Gentoo](https://emacs.ch/tags/Gentoo) and [#NixOS](https://emacs.ch/tags/NixOS) is this:

The most important value of [#Gentoo](https://emacs.ch/tags/Gentoo) is configuration/customization and reproducibility comes 2nd.

In case of NixOS those value are reversed. The most important to NixOS is ability to reproduce given setup.

Both of those systems will suit users that value control over their systems  very highly (unlike, say, Ubuntu - where the most important value is convenience), but the tie-breaking is between what value out of those two should come 1st.

<small>
    Imported via [Fedimpost](https://gitlab.com/xgqt/xgqt-csharp-app-fedimpost)
    from [emacs.ch/@xgqt/112581104037953790](https://emacs.ch/@xgqt/112581104037953790)
</small>
