    Title: GHC 9.8 and Cabal 3.10.3 on Gentoo Linux
    Date: 2024-11-07T16:02:31
    Tags: gentoo, haskell, programming language, system, tutorial

The official `::gentoo` repository currently contains only GHC on version 9.2.8.
To install newer GHC one has to either download/build themselves or use the
`::haskell` overlay (<https://github.com/gentoo-haskell/gentoo-haskell>).

## Enable the ::haskell overlay

Enable:

```shell
eselect repository enable haskell
```

Sync:

```shell
emerge --sync haskell
egencache --update --repo haskell --jobs 12 --load 6
eix-update
```

## Unmask needed packages

Add to `/etc/portage/package.unmask/0000_hs.conf`

```conf
<dev-lang/ghc-9.9

<dev-haskell/cabal-3.11
<dev-haskell/cabal-install-3.11
<dev-haskell/cabal-install-solver-3.11
<dev-haskell/cabal-syntax-3.11
<dev-haskell/text-2.2
<dev-haskell/parsec-3.1.18
```

Add to `/etc/portage/package.accept_keywords/0000_hs.conf`

```conf
app-admin/haskell-updater
dev-haskell/*
dev-lang/ghc
```

## Install

```shell
emerge --ask --verbose ">=dev-lang/ghc-9.8" ">=dev-haskell/cabal-install-3.10"
```

Build of GHC 9.8 takes around ~2 hours on a 8-core laptop-grade CPU.

## Bonus: masking packages from ::haskell

If you want to exclude a given version from the `::haskell` overly from being
installed/updated, then you can add a similar line(s) to
`/etc/portage/package.mask/0000_hs.conf`:

```shell
app-emacs/haskell-mode::haskell
```
