    Title: Debugging Frog blog with syntax macros
    Date: 2023-07-15T18:24:16
    Tags: blog, language, lisp, macro, programming, racket, scheme, tutorial

# Constructing debugging syntax

I wanted to echo parameter values when I set them in my blog's `frog.rkt`
config file.

Nothing simpler in Racket!

First I create this macro for echoing a single parameter value when it is set:

```racket
(define-syntax-rule (verbose-set-parameter parameter-id parameter-value)
  (begin
    ;; Set the parameter.
    (parameter-id parameter-value)

    ;; Then call the parameter and print is's value.
    ;; The "'parameter-id" is special syntax
    ;; for turning a "parameter-id" identifier to a symbol.
    ;; We can also write it like:
    ;; > (quote parameter-id)
    ;; to be less confusing.
    (printf "[DEBUG] (~a ~v)\n" 'parameter-id (parameter-id))))
```

then, I create a wrapper for above macro that can take multiple parameter pairs:

```racket
(define-syntax-rule (verbose-set-parameters (parameter-id parameter-value) ...)
  (begin
    ;; Unpack a chain of "(parameter-id parameter-value)" pairs
    ;; using the "..." syntax.
    (verbose-set-parameter parameter-id parameter-value) ...))
```

# Using the macro

Afterwards we can call it like so:

```racket
(verbose-set-parameters
 (current-title "XGQT's blog")
 (current-author "Maciej Barć"))
```

Notice that even the form of setting a parameter, that is
`(parameter-procedure "value")`, remains the same, but in reality it is just
similar to how the syntax macro pattern-matches on it.

# Inspecting macro expansion

In `racket-mode` inside GNU Emacs we can inspect the macro expansion with
`racket-expand-region`.
Stepping through the expansion provided this result:

```racket
(begin
  (begin
    (current-title "XGQT's blog")
    (printf "[DEBUG] (~a ~v)\n" 'current-title (current-title)))
  (begin
    (current-author "Maciej Barć")
    (printf "[DEBUG] (~a ~v)\n" 'current-author (current-author))))
```
