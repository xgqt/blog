    Title: Moving my Fediverse account to emacs.ch
    Date: 2023-06-18T00:02:37
    Tags: fediverse, rss

I have moved my main Fediverse account from
[@xgqt@fosstodon.org](https://fosstodon.org/@xgqt)
to [@xgqt@emacs.ch](https://emacs.ch/@xgqt).

The new account's RSS feed can be found at
[emacs.ch/users/xgqt.rss](https://emacs.ch/users/xgqt.rss).
