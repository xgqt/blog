    Title: Safer Nix installation
    Date: 2024-02-12T21:28:31
    Tags: linux, nix, packaging, sandbox, shell, system, test, testing, tutorial

Nix is useful for quickly testing out software and providing a strict
environment that can be shared between people.

Today I'm trying out Nix *again*, this time I want to do it **my way**.

## Installation process

### Nix store

I know Nix needs "Nix store" installation on `/` (the system root).

Create it manually to prevent the installation script from calling `sudo`.
1st I switch to the root account, and then I run:

```shell
mkdir -p -m 0755 /nix
chown -R xy:xy /nix
```

### Running the install script

Download the Nix install script and examine the contents.

```shell
curl -L https://nixos.org/nix/install > nix_install.sh
```

Then, run it with `--no-daemon` to prevent it running as system service.

```shell
sh ./nix_install.sh --no-daemon
```

    performing a single-user installation of Nix...
    copying Nix to /nix/store...
    installing 'nix-2.20.1'
    building '/nix/store/1ahlg3bviy174d6ig1gn393c23sqlki6-user-environment.drv'...
    unpacking channels...
    modifying /home/xy/.bash_profile...
    modifying /home/xy/.zshenv...
    placing /home/xy/.config/fish/conf.d/nix.fish...

    Installation finished!  To ensure that the necessary environment
    variables are set, either log in again, or type

    . /home/xy/.nix-profile/etc/profile.d/nix.fish

    in your shell.

Wait!

    modifying /home/xy/.bash_profile...
    modifying /home/xy/.zshenv...
    placing /home/xy/.config/fish/conf.d/nix.fish...

That's very rude!

### Stopping Nix from making a mess

I need to prevent Nix from mess up with my environment when I do not want it
to.
Nix puts some code into the Bash, ZSH and Fish initialization files during
installation to ease it's use.
I do not want that since I do not want Nix to meddle with my environment
without me knowing it.

I keep my `.bash_profile` and `.zshenv` in a stow-managed git repo so I can
just `cd` into my repo and do `git reset --hard`, but for you will have to
revert those files to their old forms manually.

## Playing with Nix

We do not have `nix` in `PATH` but we still can launch it.
Nix executables are located inside `~/.nix-profile/bin/`.

By invoking `nix-shell` one can create a ephemeral environment containing only
packages specified after the `-p` flag.
I always add `-p nix` to have the Nix tools available also inside the spawned
environment.

I will test out
`chibi` (small Scheme interpreter) +
`rlwrap` (REPL support for software lacking it)
inside a Nix ephemeral environment:

```shell
~/.nix-profile/bin/nix-shell -p nix chibi rlwrap
```

Inside the spawned shell:

```shell
rlwrap chibi-scheme
```

In the chibi REPL, let's see the contents of the `PATH` environment variable:

```scheme
(get-environment-variable "PATH")
```

And exit the Scheme REPL:

```shell
(exit)
```

After the playtime, run garbage collection:

```shell
~/.nix-profile/bin/nix-collect-garbage
```
