    Title: systemd-custom-unit
    Date: 2022-04-08T00:00:00
    Tags: system, systemd, tutorial

# Template

File: `/etc/systemd/system/APP.service`

``` conf
[Unit]
Description=Run APP application

[Service]
Type=simple
ExecStart=/usr/bin/LANG APP_DIR/APP APP_ARGS
Restart=on-failure
User=root
WorkingDirectory=APP_DIR

[Install]
WantedBy=multi-user.target
```

Also, the application might need to reference a PID file, let systemD
know abut it via `PIDFile`.

``` conf
PIDFile=/tmp/APP.pid
```

# Example

File: `/etc/systemd/system/julia_dash_app.service`

``` conf
[Unit]
Description=Run Julia Dash application

[Service]
Type=simple
ExecStart=/usr/bin/julia /root/julia_dash_app/main.jl
Restart=on-failure
User=root
WorkingDirectory=/root/julia_dash_app

[Install]
WantedBy=multi-user.target
```
