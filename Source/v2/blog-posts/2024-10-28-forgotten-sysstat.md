    Title: Forgotten SysStat
    Date: 2024-10-28T22:35:15
    Tags: sysadmin, system, linux

SysStat is a amazing tool. In the age where telegraf and grafana are
all the rage everybody forgot about the good old `sysstat`.

## Selected command examples

- `iostat -d -p nvme0n1 3` - disk I/O for a NVME drive (`nvme0n1`),
- `sar -n DEV 3` - network throughput,
- `sar -h -r 3` - memory usage,
- `sar -P ALL 3` - CPU utlization
- `sar -q 3` - system load levels,
- `sar -A 3` - all the metrics.

## Gathered info

> qlist app-admin/sysstat | grep /usr/bin/

The app-admin/sysstat contains the following binaries and their respective
statictic fields:

* `sar`  - general utilization statistics,
* `cifsiostat` - CIFS,
* `iostat` - device input/output,
* `mpstat` - processors,
* `pidstat` - Linux tasks,
* `tapestat` - tape (yes, the real tape disks).

## Installation

### Gentoo

```shell
emerge --noreplace --verbose sys-process/cronie app-admin/sysstat
rc-service cronie start
rc-update add cronie default
```

## Files

By default (on Gentoo):
* `sa` (the collector) saves statistics to `/var/log/sa`,
* `/etc/sysstat` is the configuration file
* cron jobs are run via the `*system*` cronjob table.
