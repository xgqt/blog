    Title: Internet EU
    Date: 2020-07-05T00:00:00
    Tags: europe, european union, politics

-   I think this is quite important and wanted to share it.
-   YouTube link: <https://www.youtube.com/watch?v=WdwegN7u6x8>
-   Invidio link: <https://invidio.us/watch?v=WdwegN7u6x8>

# Polish original

> Internet stał się tym czym jest, stał się takim światem w miniaturze
> dzięki dobrowolnemu zaangażowaniu zwykłych ludzi, dzięki brakowi
> regulacji. I to was boli, bo rozwój internatu jest przykładem tego,
> jak mógłby się rozwijać nasz świat, gdyby nie te wszystkie wasze
> regulacje, gdyby nie ten ciężki plecak przepisów i podatków który
> każdy z nas musi na co dzień dźwigać. Internet to jest po prostu
> ostatni bastion wolności i to wam nie daje spać, bo to jest dowód na
> to, że wszystkie wasze regulacje i te rozporządzenia, dyrektywy, są
> tak naprawdę zbędne i szkodliwe i świat bez tego rozwijałby się
> lepiej, rozwijałby się dokładnie tak jak Internet, że gdyby nie te
> wasze właśnie ambitne plany, wartości dodane, efekty dźwigni,
> wysyłanie silnych sygnałów, to wszystko, żyłoby się nam po prostu
> lepiej i prościej. Dlatego próbujecie to zniszczyć, dlatego próbujecie
> wsadzić stopę w drzwi za każdym razem kiedy pojawia się okazja. Ale my
> nie chcemy waszej nogi w tych drzwiach. Teraz cofacie się o parę
> centymetrów, bo ludzie protestują, ale my nie chcemy was tam w ogóle,
> nie chcemy waszych regulacji, nie chcemy waszych łap w Internacie. To
> działa dobrze, twórczość rozwija się w Internecie bez żadnych waszych
> regulacji.

# English translation

> Interent has become what it is, it has become a miniature world due to
> the voluntary involvement of ordinary people, thanks to the lack of
> regulation. And this hurts you, because the development of the Intenet
> is an example of how our world could develop, without all your
> regulations, without this heavy backpack of regulations and taxes that
> each of us must carry on a daily basis. The internet is simply the
> last bastion of freedom and this is the thing that doesn\'t let you
> sleep, because it is proof that all your regulations and those
> ordinances, directives, they are really unnecessary and harmful and
> the world would develop better without it, it would develop exactly
> like the Internet, that if it weren\'t for your ambitious plans, added
> values, leverage effects, sending strong signals, then everyone would
> have lived just better and simpler. That\'s why you try to destroy it,
> that\'s why you try to stick your foot in the door whenever there is
> an opportunity. But we don\'t want your leg in our door. Now you are
> retreating a few centimeters because people are protesting, but we
> don\'t want you there at all, we don\'t want your regulations, we
> don't want your hands in the Internet. It works well, creativity
> develops on the Internet without any of your regulations.
