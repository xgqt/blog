    Title: Mkdocs with Scribble
    Date: 2022-07-12T00:00:00
    Tags: racket, programming language, scribble

# Intro

Instead of changing CSS style for Your
[Racket](https://racket-lang.org/) projects documentation, You may be
interested in compiling
[Markdown](https://en.wikipedia.org/wiki/Markdown) files generated form
[Scribble](https://docs.racket-lang.org/scribble/) source into HTML
documentation website.

# Creating MkDocs project

1.  Create `docs` directory and `mkdocs.yml`
    config file in current directory, along with a dummy
    `index.md` file in `docs` folder.

    ``` shell
    mkdocs new .
    ```

2.  Edit the name of the project.

    *Replace `Racket-Project` with your project name.*

    ``` yaml
    ---
    site_name: Racket-Project
    ```

# Building Scribble

Generate markdown files form scribble documentation.

*Replace `Racket-Project.scrbl` with path to your scribble documentation
main source file.*

``` shell
scribble --markdown --dest ./docs --dest-name index.md Racket-Project.scrbl
```

# Building Markdown

Compile HTML documentation from the markdown source.

``` shell
mkdocs build
```

HTML files should appear in the `site` directory.

# Running the server

Some features, like search for example are only available when running
the mkdocs server.

``` shell
mkdocs serve
```

# Caveats

Some scribble functions do not look good or work correctly for
markdown-to-HTML compilation by MkDocs.

-   `table-of-contents` - looks like a source block

-   `index-section` - letter links do not work

# Example configuration

``` yaml
site_name: Racket-Ebuild
site_author: xgqt@riseup.net
site_description: library to ease ebuild creation
site_url: https://gitlab.com/gentoo-racket/racket-ebuild

repo_name: gentoo-racket/racket-ebuild
repo_url: https://gitlab.com/gentoo-racket/racket-ebuild

plugins:
  - search

theme:
  name: material

extra:
  social:
    - icon: fontawesome/brands/gitlab
      link: https://gitlab.com/gentoo-racket/racket-ebuild
```
