    Title: Ebuild lit tests
    Date: 2023-02-24T00:00:00
    Tags: gentoo, ebuild, tutorial, python

# Patching

The file `lit.site.cfg` has to be inspected for any incorrect
calls to executables. For example see `src_prepare` function form
[dev-lang/boogie](https://gitweb.gentoo.org/repo/gentoo.git/tree/dev-lang/boogie).

# Eclasses

Because we will need to specify how many threads should `lit`
run we need to inherit `multiprocessing` to detect how many parallel
jobs the portage config sets.

``` shell
inherit multiprocessing
```

# Dependencies

Ensure that `dev-python/lit` is in `BDEPEND`, but also
additional packages may be needed, for example
`dev-python/OutputCheck`.

``` shell
BDEPEND="
    ${RDEPEND}
    test? (
        dev-python/lit
        dev-python/OutputCheck
    )
"
```

# Bad tests

To deal with bad test you can simply remove the files causing the
failures.

``` shell
local -a bad_tests=(
    civl/inductive-sequentialization/BroadcastConsensus.bpl
    civl/inductive-sequentialization/PingPong.bpl
    livevars/bla1.bpl
)
local bad_test
for bad_test in ${bad_tests[@]} ; do
    rm "${S}"/Test/${bad_test} || die
done
```

# Test phase

`--threads $(makeopts_jobs)` specifies how many parallel tests to run.

`--verbose` option will show output of failed tests.

Last `lit` argument specifies where `lit` should
look for `lit.site.cfg` and tests.

``` shell
src_test() {
    lit --threads $(makeopts_jobs) --verbose "${S}"/Test || die
}
```
