    Title: HTML Renderers
    Date: 2021-05-04T00:00:00
    Tags: browser, emacs

# How it should be?

I imagine that a web browser should have been a \"window\" to the
Internet world that provides easy and efficient way to graphically
access resources exposed by multiple protocols: HTTP(S), (S)FTP, Gopher,
Gemini, etc...

# How it is?

**Only few protocols are supported!**

Recently FTP support got \"deprecated\" in Firefox. It can still be
enabled in <about:config> by setting `network.ftp.enabled` to
**true**.

FTP support is announced to be completely gone in FF 90.

Chrome dropped FTP some time ago.

# BUT!

Luckily we have **Emacs**!

GNU Emacs has packages to support all kinds of protocols.

-   `eww` which is a simple HTTP(S) browser like w3m (also
    renders images)
-   `tramp` allows you to access files by SSH
-   `elpher` allows you to access Gemini and Gopher sites
    (graphically)
-   `ange-ftp` allows you to connect to FTP servers
-   and finally `net-utils` which **wraps** around **system**
    utilities to provide interactive mode for many protocols, ie.:
    gopher, irc, ntp, pop3, www
