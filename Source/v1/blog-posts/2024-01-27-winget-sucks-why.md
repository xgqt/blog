    Title: winget sucks. Why?
    Date: 2024-01-27T05:06:47
    Tags: microsoft, packaging, sysadmin, windows, winget

I would have stopped myself from writing about winget but far too many times
when I do a full package set upgrade some package does not install correctly.

## Running too many install hooks

Sometimes package installation may fail because
"same version of the package is installed"...
**Wait! What?**
How is the version I am updating already installed?
Well I got this bizarre error (that probably indicates a more serious bug)
when trying to update Docker Desktop via winget.

What often happens is that because winget runs the package install / update
executable, that is a binary, we do not know exactly what it could do,
want or expect. So, what you might get is that the software actually expects
you to invoke the updater in a different way.

## Not enough installer checks

This is a even more annoying bug that, after executing the installer
the package in question package does not register itself.
I have hit this bug with a package very crucial to me - GNU Emacs.

It is true that this should be fixed by the upstream project because
the installer is just broken, but most of those bugs are known.
So, is there no mechanism that we can use to register GNU Emacs ourselves?
I just do not get why is there no check to see if the package is **actually**
installed or uninstalled. Honestly quite insane!

## Proper CI/CD

I think the winget issues can be mitigated by implementing a proper CI/CD for
packages.
Below I propose a system that will test packages in two clean runs:

1. Installation checks.

    This step would consist of 3 actions to determine correct installation
    and removal of the package:

    * install the package and check if it installed correctly
    * install the package again,
      if the package registered correctly during the previous step,
      then no installation phase should run
      and this step would catch the mentioned beforehand GNU Emacs bug
    * uninstall the package to check if it is removed correctly

2. Update test.

    Very simple - two steps: install older version of a package and hen attempt
    to update it. I guarantee lot of packages will not pass this with
    out-of-the-box configuration.
