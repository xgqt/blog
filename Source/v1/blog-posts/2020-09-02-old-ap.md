    Title: Old Access Point
    Date: 2020-09-02T00:00:00
    Tags: hardware, router, access point

# Introduction

On some old routers, namely TP-Link\'s TL-WR840N version 2, there may
not be a option to switch to access point mode. This is what you have to
do to access that mode indirectly.

# Steps

-   Power off the router
-   Unplug RJ cable from the WAN port
-   Connect the router to a PC (using one of the LAN ports)
-   Power on the router
-   Log it to the web console entering your router\'s IP, you can find
    out the IP by executing `ip --color a` (on a Linux box)
    if the router\'s DHCP server is still active
-   In LAN settings give your router a static client IP that fits into a
    network you want to connect it to; example: 192.168.100.100/24 (make
    sure no other hosts are associated with that IP in the target
    network)
-   Restart the router
-   Log in to the web console entering the IP you have set
-   Turn off the DHCP server (DHCP -\> DHCP Settings -\> DHCP Server:
    Disable & Save)
-   Power off the router
-   Plug the cable from a network you want the router connected to to
    one of the LAN ports
-   Power on the router
-   Log in to the web console entering the IP you have set
-   Turn on the wireless network and set it up (SSID, password, etc.)
-   Done! :D

# Sources

-   <https://www.tp-link.com/in/support/download/tl-wr840n/v2/>
-   <https://www.youtube.com/watch?v=Cg_gGECGLiY>
