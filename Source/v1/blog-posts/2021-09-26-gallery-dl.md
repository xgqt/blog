    Title: Make gallery-dl work like youtube-dl
    Date: 2021-09-26T00:00:00
    Tags: customization

# Make gallery-dl work like youtube-dl

A configuration file to download everything into current directory, like
yotube-dl does.

File: `~/.config/gallery-dl/config.json`

``` json
{
  "directory": [],
  "base-directory": "./"
}
```
