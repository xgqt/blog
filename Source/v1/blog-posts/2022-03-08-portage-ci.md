    Title: Portage CI
    Date: 2022-03-08T00:00:00
    Tags: continuous integration, gentoo, portage, quality assurance, testing

# Potential benefits

## Running tests

-   test BEFORE (`src_test`) and AFTER
    (`pkg_postinst`) installation
-   test if and how services break if they are not reloaded
-   test buildsystem configuration
-   sandbox enforces strict and consistent build rules
-   benchmarking with different compilation flags and libraries
    versions/releases

## Configuration matrix

We can test across Cartesian product of different configuration
settings, like:

-   [USE flags](https://wiki.gentoo.org/wiki/USE_flag)
-   MAKEOPTS
-   CFLAGS, CXXFLAGS, CPPFLAGS, LDFAGS, RUSTFLAGS, etc.
-   arches (cross-compilation or run in qemu)
-   static linking
-   supported releases & versions of libraries (eg. glibc & musl)

Also, we could create diffs of installed files across different merges.

## Reproducibility

-   mini [overlay](https://wiki.gentoo.org/wiki/Ebuild_repository) with
    ::gentoo or any other (eg. company\'s own) as master
-   record VCS (eg. git) hash of the dependent overlays

## Binaries

-   grab dependencies from
    [binhosts](https://wiki.gentoo.org/wiki/Binary_package_guide#Setting_up_a_binary_package_host)
-   distribute built binaries (maybe upload to a company\'s own
    artifacts server)
-   make [AppImage](https://appimage.org/)s

# Getting there

## How do we run this?

Do we want to write a proper tool, which we probably do or do we just
run Portage + shells scripts?

Do we want to run under root, user, in
[eprefix](https://wiki.gentoo.org/wiki/Project:Prefix), maybe all in
docker?

## Configuration files

The `.portci` directory contains the configuration.

## Bug 799626

Link: [bugs.gentoo.org/799626](https://bugs.gentoo.org/799626)

Instead of using Ansible, Python, Yaml or Scheme we might use something
similar to this for simple configuration, or if gets merged to upstream
Portage the better.

Worth mentioning is the idea from Michał Górny who proposes to configure
portage with toml files, like the example given in the bug report.

``` conf
[package.unmask]
~virtual/libcrypt-2

[package.use.mask]
sys-libs/libxcrypt -system -split-usr

[package.use.force]
sys-libs/glibc -crypt
```

Also, `package.x` + Toml == a match made in heaven, it looks
very nice!
