    Title: Time hackery in VirtualBox
    Date: 2023-07-08T00:39:44
    Tags: hacking, virtualbox, virtualization, vm, windows

# Unwanted (VDI) present

Recently I was given a task to run a outdated piece of commercial software
on a very outdated (in)famous operating system.
Unlucky for me on the given VDI (VirtualBox disk image) the so-called
"Guests Additions" that enabled "better" time synchronization were already
installed.

# Fighting VBox time

So, this is what I had to do to keep the old clock:

* turn off networking card in the virtual machine configuration,
  because we do not want the system to use NTP or other newt service to get
  the "new" time,
* disable getting the host's clock time:
  `VBoxManage setextradata "WinXP"
      "VBoxInternal/Devices/VMMDev/0/Config/GetHostTimeDisabled" 1`,
* and... this magic:
  `VBoxManage modifyvm "WinXP"
      --biossystemtimeoffset "-341597644449"`,
  this thing does the very weird thing of telling the virtual machine
  BIOS how much to set it back in time,
  the `offset` here is total milliseconds
  between some old date and current date.

# Getting the old date

With the Python script snippet below you will get the milliseconds to set back
the VirtualBox clock to `01.01.2012 01:00`.

```python
from datetime import datetime

round((datetime.strptime("01.01.2012 01:00", "%d.%m.%Y %H:%M")
      - datetime.now()).total_seconds() * 1000)
```
