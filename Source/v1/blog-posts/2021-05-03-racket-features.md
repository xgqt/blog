    Title: Awesome Racket language features
    Date: 2021-05-03T00:00:00
    Tags: racket, programming language

Also see: [Fast-Racket at Racket\'s GitHub Wiki](https://github.com/racket/racket/wiki/Fast-Racket)

# Creating binaries

You can create **portable** binaries with Racket\'s `raco`
command! Use `raco exe` and `raco distribute`.

More -\> <https://docs.racket-lang.org/raco/exe.html>

# Sample games

Racket provides a executable `plt-games`, when ran (from
console) it opens a menu of miscellaneous games, among them: jewel,
minesweeper, aces, spider, checkers. & more (20 games total).

# Plots

You can [plot](https://docs.racket-lang.org/plot/index.html) data in
[2d](https://docs.racket-lang.org/plot/intro.html#%28part._.Plotting_2.D_.Graphs%29)
&
[3d](https://docs.racket-lang.org/plot/intro.html#%28part._.Plotting_3.D_.Graphs%29)
forms.

## 2D

Sample code:

``` racket
#lang racket/base
(require racket/gui/base racket/math plot)

(plot-new-window? #true)

(plot (function sin (- pi) pi #:label "y = sin(x)"))
```

## 3D

Sample code:

``` racket
#lang racket/base
(require racket/gui/base racket/math plot)

(plot-new-window? #true)

(plot3d
 (surface3d (lambda (x y) (* (cos x) (sin y)))
            (- pi) pi (- pi) pi)
 #:title "An R × R → R function"
 #:x-label "x" #:y-label "y" #:z-label "cos(x) sin(y)")
```

# Browser

There\'s a included library to render web pages, just \"[(require
browser)](https://docs.racket-lang.org/browser/index.html)\".

Sample code:

``` racket
#lang racket
(require browser)

(open-url "https://xgqt.gitlab.io/")
```

# FFI

You can use Racket\'s Foreign Function Interface to interact with
non-Racket libraries to make use of very fast libraries written in
(mainly) FORTRAN & C.

For example [sci](https://github.com/soegaard/sci) uses FFI for [CBLAS &
LAPACK](http://www.netlib.org/blas/).

# Parallelism

For greater speed up with parallel execution there are
[futures](https://docs.racket-lang.org/reference/futures.html),
[places](https://docs.racket-lang.org/reference/places.html) and
[distributed
places](https://docs.racket-lang.org/distributed-places/index.html) (for
distributed programming).
