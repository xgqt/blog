    Title: Bubblewrap cross-architecture chroot
    Date: 2023-04-28T17:06:33
    Tags: chroot, emulation, gentoo, linux, sandbox, system, tutorial, virtualization, vm

# System preparation

## Qemu

Emerge `qemu` with `static-user` USE enabled and your wanted architectures.

```conf
app-emulation/qemu      QEMU_SOFTMMU_TARGETS: aarch64 arm x86_64
app-emulation/qemu      QEMU_USER_TARGETS: aarch64 arm x86_64

app-emulation/qemu      static-user
dev-libs/glib           static-libs
sys-apps/attr           static-libs
sys-libs/zlib           static-libs
dev-libs/libpcre2       static-libs
```

## OpenRC

Enable `qemu-binfmt`:

```shell
rc-update add qemu-binfmt default
```

Start `qemu-binfmt`:

```shell
rc-service qemu-binfmt start
```

# Chrooting

- select chroot location
  (eg `/chroots/gentoo-arm64-musl-stable`)
- unpack the desired rootfs
- create needed directories
    - `mkdir -p /chroots/gentoo-arm64-musl-stable/var/cache/distfiles`
- execute `bwrap`
    - with last `ro-bind` mount the qemu emulator binary
      (eg `qemu-aarch64`)
    - execute the mounted emulator binary giving it a shell program
      (eg `bash`)

Chroot with `bwrap`:

```shell
bwrap                                                       \
    --bind /chroots/gentoo-arm64-musl-stable /              \
    --dev /dev                                              \
    --proc /proc                                            \
    --perms 1777 --tmpfs /dev/shm                           \
    --tmpfs /run                                            \
    --ro-bind /etc/resolv.conf /etc/resolv.conf             \
    --bind /var/cache/distfiles /var/cache/distfiles        \
    --ro-bind /usr/bin/qemu-aarch64 /usr/bin/qemu-aarch64   \
    /usr/bin/qemu-aarch64 /bin/bash -l
```
