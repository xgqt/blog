    Title: Portage system replication
    Date: 2020-11-14T00:00:00
    Tags: gentoo, portage, sysadmin, system

# Intro

Backing up using this method takes a lot less space - \~60MB (without
distfiles) and can be restored on almost any system (running portage)
and tweaked afterwards for, say, CPU architecture. I\'ve created a a
short script with similar method in
[here](https://gitlab.com/xgqt/mydot/-/blob/master/scripts/.local/share/bin/bak-portage).

# What we need

-   ebuild repositories are installed with git
-   distfiles (those might be gone when we want to replicate)

# Backup

``` shell
# System info
emerge --info > info.txt

# Portage tree
cp -Lr /etc/portage .

# Portage layout
tree -a -L 2 /etc/portage > layout.txt

# Packages in @world
cp /var/lib/portage/world .

# Installed sets
cp /var/lib/portage/world_sets .

# Installed packages (with versions)
qlist --installed --nocolor --umap > qlist-use.txt
qlist --installed --nocolor --verbose > qlist-ver.txt

# Distfiles
cp -rv "$(portageq envvar DISTDIR)" distfiles

# Ebuild database
cp -r /var/db/pkg pkgdb
```

# Restoration

To faithfully restore the system perform those actions as root

``` shell
# Copy the portage tree to /etc
rm -dr /etc/portage
cp -r portage /etc/portage

# Checkout the gentoo repo to a commit specified in info.txt
cd "$(portageq get_repo_path / gentoo)"
git checkout # <commit ID>

# Copy distfiles
cp -r distfiles/* "$(portageq envvar DISTDIR)"/

# Fake-install @world and sets
cp world /var/lib/portage/world
cp world_sets /var/lib/portage/world_sets

# Emerge the exact packages from qlist-ver.txt
emerge --keep-going=y -1Oav $(sed 's/^/=/' qlist-ver.txt)
```
