    Title: KDE Emacs
    Date: 2022-08-26T00:00:00
    Tags: kde, emacs, tutorial, customization

# Proper window size

Sometimes while the Emacs GUI window is tiled to a side or maximized
small gaps may appear around the window. This \"bug\" can be worked
around by:

-   right-click on the title bar,
-   \"More Actions\",
-   \"Configure Special Window Settings...\",
-   \"Add Property\",
-   \"Obey Geometry Restrictions\",
-   Select \"Force\" form the combo box,
-   Select \"No\" from the radio buttons.

# Opening files from Dolphin in one Emacs instance

Emacs daemon can help with that. But before you run `emacs --daemon`, I
need You to know that there might be a better way:

``` elisp
(unless (or noninteractive (server-running-p))
  (server-start))
```

Adding the above to Your Emacs config will cause Emacs to start a daemon
after it is opened (and no other Emacs servers are running), this also
does not require `--daemon` flag.

After the daemon is started You can open files by right-clicking on them
and selecting to open them in \"Emacsclient\".

Furthermore: You also utilize `--iconic` and add `emacs --iconic` to
your Plasma startup. This is way better than using `emacs --daemon`
because you can just click on your taskbar to open the minimized Emacs
window. Also, Emacs will load all Your graphical libraries and
configurations so Your theme will look properly and not as if Emacs was
being used on the console.

# Breeze theme

Sadly I have not found any theme that would look like Plasma. I use the
[spacemacs theme](https://github.com/nashamri/spacemacs-theme/) which
looks a little bit similar, especially the background color comes close
to Breeze\'s dark background color.

Note that the theme which You load with the function `load-theme` is a
different thing that the GTK theme Emacs uses.

The GTK theme should be enabled if Your Emacs version is built with GTK
support. On Gentoo this setting is controlled with the `gtk` USE flag.
Also the flag `toolkit-scroll-bars` can be enabled for a look of
scroll-bars consistent with the selected toolkit.

## Xresources

There is a different approach to theming Your Emacs that loading a theme
defined in ELisp - You can use a `~/.Xresource` file.

If you do not load any theme in your configuration Emacs will by default
read the .Xresources file, unless the `--no-x-resources` flag is used.

Here are a few Xresources config files that come close to the default
Breeze theme:

-   <https://github.com/kuriot/xresources-breeze-theme/blob/master/.Xresources>
-   <https://gitcode.net/mirrors/mbadolato/iTerm2-Color-Schemes/-/blob/master/Xresources/Breeze>

# Dbus integration

Emacs can be built with [FreeDesktop\'s
D-Bus](https://www.freedesktop.org/wiki/Software/dbus/) support to
communicate over the dbus protocol. This can come handy when using ERC
as it has a setting to enable desktop notifications on mentions
([erc-desktop-notifications.el](https://github.com/emacs-mirror/emacs/blob/master/lisp/erc/erc-desktop-notifications.el)).

The dbus interface can also be utilized to query desktop-oriented
daemons, for example [this
library](https://github.com/emacsmirror/bluetooth/) talks to the
Bluetooth daemon.

# KDE development

[Those](https://github.com/KDE/kde-dev-scripts/tree/master/kde-emacs/)
are some ELisp libraries that I found while browsing GitHub, they might
be useful for somebody who delves into KDE app development.

# Opening files in different applications

In addition to `async-shell-command` and `start-process-shell-command` I
wrote
[this](https://gitlab.com/xgqt/mydot/-/raw/186da73f9e6f88978320cfb3d791ccbc8efcbe70/src/emacs/.config/emacs/assets/site-lisp/open-with.el)
small library that may come handy.

# Outside Emacs, inside Plasma

Sadly the KDE team did not add support to emulate Emacs-like keys in
Plasma itself, but some applications like, for example
[Kate](https://kate-editor.org/) have configuration options to customize
the key bindings. [This](https://github.com/evanpeterjones/kate-macs/)
is a repository explaining how to setup Kate\'s bindings.
