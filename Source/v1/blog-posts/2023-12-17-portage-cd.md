    Title: Portage Continuous Delivery
    Date: 2023-12-17T00:50:15
    Tags: gentoo, linux, sysadmin, system

## Portage as a CD system

This is a very simple way to use any system with Portage installed as
a Continuous Delivery server.

I think for a testing environment this is a valid solution to consider.

### Create a repository of software used in your organization

Those articles from the Gentoo Wiki describe how to create a custom ebuild
repository (overlay) pretty well:

* [Ebuild repository](https://wiki.gentoo.org/wiki/Ebuild_repository)
* [Creating an ebuild repository](https://wiki.gentoo.org/wiki/Creating_an_ebuild_repository)

### Set up your repo with eselect-repository

Install the `my-org` repository:

```shell
eselect repository add my-org git https://git.my-org.local/portage/my-org.git
```

Sync `my-org`:

```shell
emerge --sync my-org
```

### Install live packages of a your software

First, enable live packages (keywordless) for your `my-org` repo:

```shell
echo '*/*::my-org' >> /etc/portage/package.accept_keywords/0000_repo_my-org.conf
```

Install some packages from `my-org`:

```shell
emerge -av "=mycategory/mysoftware-9999"
```

### Install smart-live-rebuild

`smart-live-rebuild` can automatically update live software packages that use
git as their source URL.

### Set up cron to run smart-live-rebuild

Refresh your `my-org` repository every hour:

```cron
0 */1 * * * emerge --sync my-org
```

Refresh the main Gentoo tree every other 6th hour:

```cron
0 */6 * * * emerge --sync gentoo
```

Run `smart-live-rebuild` every other 3rd hour:

```cron
0 */3 * * * smart-live-rebuild
```

## Restarting services after update

### All-in-one script

You can either restart all services after successful update:

File: `/opt/update.sh`

```shell
#!/bin/sh

set -e

smart-live-rebuild

systemctl restart my-service-1.service
systemctl restart my-service-2.service
```

Crontab:

```cron
0 */3 * * * /opt/update.sh
```

### Via ebuilds pkg_ functions

File: `my-service-1.ebuild`

```shell
pkg_postinst() {
    systemctl restart my-service-1.service
}
```

More about `pkg_postinst`:

* [pkg_postinst](https://devmanual.gentoo.org/ebuild-writing/functions/pkg_postinst/)
* [Phase Functions](https://devmanual.gentoo.org/ebuild-writing/functions/index.html)

## Example Gentoo overlays

* [xgqt/myov](https://gitlab.com/xgqt/myov/)
* [gentoo-racket/gentoo-racket-overlay](https://gitlab.com/gentoo-racket/gentoo-racket-overlay/)
* [gentoo-mirror/guru](https://github.com/gentoo-mirror/guru/)
