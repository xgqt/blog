    Title: ELisp ebuilds good practices
    Date: 2023-07-15T19:25:25
    Tags: elisp, emacs, gentoo, lisp, packaging

# Check load path

Some Elisp package compilation failures are caused by not setting the loadpath
correctly.
It mostly happens when you compile source from a directory that is not the
current working directory.
For example:

```bash
elisp-compile elisp/*.el
```

In most cases you can `cd` or override the `S` variable to set it to location
where ELisp source resides.

But in other cases you can append to load path the directory with source,
see:

```bash
BYTECOMPFLAGS="${BYTECOMPFLAGS} -L elisp" elisp-compile elisp/*.el
```

# Do not rename auto-generated autoload file

`elisp-make-autoload-file` allows to name the generated autoload file.
For sake of easier debugging and writing Gentoo `SITEFILE`s,
please do not rename the generated file.

The name of that file should always be `${PN}-autoloads.el`.

# Use new elisp-enable-tests function

`elisp-enable-tests` allows to set up `IUSE`, `RESTRICT`, `BDEPEND`
and the test runner function for running tests with the specified test runner.

The 1st (`test-runner`) argument must be one of:

* `buttercup` -- for `buttercup` provided via `app-emacs/buttercup`,
* `ert-runner` -- for `ert-runner` provided via `app-emacs/ert-runner`,
* `ert` -- for ERT, the built-in GNU Emacs test utility.

The 2nd argument is the directory where test are located,
the leftover arguments are passed to the selected test runner.

Example:

```bash
EAPI=8

inherit elisp

# Other package settings ...

SITEFILE="50${PN}-gentoo.el"
DOCS=( README.md )

elisp-enable-tests buttercup test
```

# Remove empty SITEFILEs

Recently a feature was added to `elisp.eclass` that will cause build process
to generate the required `SITEFILE` with boilerplate code if it does not exist.

So if your `SITEFILE` looked like this:

```lisp
(add-to-list 'load-path "@SITELISP@")
```

... then, you can just remove that file.

But remember to keep the `SITEFILE` variable inside your ebuild:

```bash
SITEFILE="50${PN}-gentoo.el"
```

# Remove pkg.el files

The `*-pkg.el` files are useless to Gentoo distribution model of Emacs Lisp
packages and should be removed.
It is as simple as adding this line to a ebuild:

```bash
ELISP_REMOVE="${PN}-pkg.el"
```

Beware that some packages will try to find their `${PN}-pkg.el` file,
but in most cases this will show up in failing package tests.

# Use official repository

It is tedious to repackage Elpa tarballs, so use the official upstream even
if you have to snapshot a specific commit.

To snapshot GitHub repos you would generally use this code:

```bash
# First check if we have the correct version to prevent
# autobumping package version without changing the commit.
[[ ${PV} == *_p20220325 ]] && COMMIT=65c496d3d1d1298345beb9845840067bffb2ffd8

# Use correct URL that supports snapshots.
SRC_URI="https://github.com/domtronn/${PN}/archive/${COMMIT}.tar.gz
    -> ${P}.tar.gz"

# Override the temporary build directory variable.
S="${WORKDIR}"/${PN}-${COMMIT}
```

# Include live version support

We do not want to be worse than the [Melpa unstable](https://melpa.org/) :D

So, why not allow the given package to be used live?

Even if you do not push the live package to the overlay,
please include support for it.

```bash
if [[ ${PV} == *9999* ]] ; then
    inherit git-r3
    EGIT_REPO_URI="https://github.com/example/${PN}.git"
else
    SRC_URI="https://github.com/example/${PN}/archive/${PV}.tar.gz
        -> ${P}.tar.gz"
    KEYWORDS="~amd64 ~x86"
fi
```

# Ask for tags

Git is good, git tags are good. In case if upstream does not tag their package
or just forgets to, kindly ask them to create a git tag when bumping Emacs
package versions.
