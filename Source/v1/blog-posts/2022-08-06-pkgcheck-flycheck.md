    Title: Pkgcheck-Flycheck
    Date: 2022-08-06T00:00:00
    Tags: emacs, pkgcheck, gentoo, ebuild, tutorial

# News

## Repository

With [this
commit](https://github.com/pkgcore/pkgcheck/commit/9089bd15a89db904e62a2b1a5db6c24b281676a0/)
first [GNU Emacs](https://www.gnu.org/software/emacs/) integration was
merged into the [pkgcheck](https://pkgcore.github.io/pkgcheck/)
repository.

## History

-   <https://github.com/pkgcore/pkgcheck/issues/417>
-   <https://github.com/pkgcore/pkgcheck/pull/420>
-   <https://github.com/gentoo/gentoo/pull/26700>

## Thanks

Huge thanks to [Sam James](https://github.com/thesamesam/) and [Arthur
Zamarin](https://github.com/arthurzam/) for support and interest in
getting this feature done.

# Installation

## Unmasking

The Flycheck integration is unreleased as of now, this will (hopefully)
change in the future, but for now You need live versions of `snakeoil`,
`pkgcore` and `pkgcheck`.

File: `/etc/portage/package.accept_keywords/pkgcore.conf`

``` conf
dev-python/snakeoil  **
sys-apps/pkgcore     **
dev-util/pkgcheck    **
```

Also You will need to unmask `app-emacs/flycheck` and its dependencies.

File: `/etc/portage/package.accept_keywords/emacs.conf`

``` conf
app-emacs/epl
app-emacs/pkg-info
app-emacs/flycheck
```

## Emerging

Install pkgcheck with the `emacs` USE flag enabled.

File: `/etc/portage/package.use/pkgcore.conf`

``` conf
dev-util/pkgcheck    emacs
```

Afterwards run:

``` shell
emerge -1av dev-python/snakeoil sys-apps/pkgcore dev-util/pkgcheck
emerge -av --noreplace dev-util/pkgcheck
```

# Configuration

Following is what I would suggest to put into your Emacs config file:

``` elisp
(require 'ebuild-mode)
(require 'flycheck)
(require 'flycheck-pkgcheck)

(setq flycheck-pkgcheck-enable t)

(add-hook 'ebuild-mode-hook 'flycheck-mode)
(add-hook 'ebuild-mode-hook 'flycheck-pkgcheck-setup)
```

If You are using `use-package`:

``` elisp
(use-package flycheck
  :ensure nil)

(use-package ebuild-mode
  :ensure nil
  :hook ((ebuild-mode . flycheck-mode)))

(use-package flycheck-pkgcheck
  :ensure nil
  :custom ((flycheck-pkgcheck-enable t))
  :hook ((ebuild-mode . flycheck-pkgcheck-setup)))
```

The lines with `:ensure nil` are there to prevent `use-package` from
trying to download the particular package from Elpa (because we use
system packages for this configuration).
