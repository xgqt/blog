    Title: Chromium
    Date: 2022-09-02T00:00:00
    Tags: customization

# Dark interface

Force web UI dark mode

``` shell
--enable-features=WebUIDarkMode --force-dark-mode
```

# Hardware acceleration

Accelerated video decoding and GPU support

``` shell
--enable-accelerated-video-decode --enable-gpu
```

# Reader mode

``` shell
--enable-reader-mode
```

# Runtime cache

Use the cache directory /run/user/1000/chrome/cache

``` shell
--disk-cache-dir=${XDG_RUNTIME_DIR}/chrome/cache
```

# Window size

Sawn a window of size 1200x900

``` shell
--window-size=1200,900
```

# Full config

The file `/etc/chromium/default` is sourced by the Chromium
launcher, that\'s why we can sue bash syntax here.

``` shell
#!/usr/bin/env bash

# Dark interface
# Force web UI dark mode
CHROMIUM_FLAGS="${CHROMIUM_FLAGS} --enable-features=WebUIDarkMode --force-dark-mode"

# Hardware acceleration
# Accelerated video decoding and GPU support
CHROMIUM_FLAGS="${CHROMIUM_FLAGS} --enable-accelerated-video-decode --enable-gpu"

# Reader mode
CHROMIUM_FLAGS="${CHROMIUM_FLAGS} --enable-reader-mode"

# Runtime cache
# Use the cache directory /run/user/1000/chrome/cache
CHROMIUM_FLAGS="${CHROMIUM_FLAGS} --disk-cache-dir=${XDG_RUNTIME_DIR}/chrome/cache"

# Window size
# Sawn a window of size 1200x900
CHROMIUM_FLAGS="${CHROMIUM_FLAGS} --window-size=1200,900"
```

# Binary

If you use `www-client/chromium-bin`, then the config is located at
`/etc/chromium/default` and `CHROMIUM_FLAGS` is `CHROMIUM_BIN_FLAGS`.
