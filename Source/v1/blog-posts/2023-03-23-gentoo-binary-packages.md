    Title: Binary packages in Gentoo
    Date: 2023-03-23T09:01:04
    Tags: binary packages, gentoo, packaging, portage, system

# Binpkgs generated by user

The binary packages generated by user can have architecture-specific
optimizations because they are generated after they were compiled by the host
Portage installation.

In addition binpkgs are generated from ebuilds so if there is a <u>USE</u> flag
incompatibility on the consumer system then the binpkg will not be installed on
the host and Portage will fall back to <u>from-source</u> compilation.

Those binary packages can use two formats:
[XPAK](https://wiki.gentoo.org/wiki/Binary_package_guide#XPAK_format)
and
[GPKG](https://www.gentoo.org/glep/glep-0078.html).

XPAK had many issues and is getting superseded by the GPKG format.
Beware of upcoming GPKG transition and if you **must** use XPAKs then you
should explicitly enable it in your system's Portage configuration.

To host a binary package distribution server see the
[Binary package guide](https://wiki.gentoo.org/wiki/Binary_package_guide)
on the Gentoo wiki.

# Bin packages in a repository

Binary packages in `::gentoo` (the official Gentoo repository) have the
<u>-bin</u> suffix.

Those packages might have USE flags but generally they are very limited in case
of customizations or code optimizations because they were compiled either by
a Gentoo developer or by a given package <u>upstream</u> maintainer
(or their CI/CD system).

Those packages land in `::gentoo` mostly because it is too hard
(or even impossible) to compile them natively by Portage.
Most of the time those packages use very complicated build systems or do not
play nice with network sandbox like (e.g. Scala-based projects) or use
very large frameworks/libraries like (e.g. <u>Electron</u>).

They can also be added to the repository because they are very <u>desirable</u>
either by normal users
(e.g. [www-client/firefox-bin](https://packages.gentoo.org/packages/www-client/firefox-bin))
or for (from-source) package <u>bootstrapping</u> purposes
(e.g. [dev-java/openjdk-bin](https://packages.gentoo.org/packages/dev-java/openjdk-bin)).
Such packages are sometimes generated from the regular source packages inside
`::gentoo` and later repackaged.
