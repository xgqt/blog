#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

source_root="$(realpath "${script_root}/../")"
echo "[$(date +'%Y-%m-%dT%H:%M:%S') INF] Entering directory: ${source_root}"
cd "${source_root}"

exec make -C "${source_root}/blog-frog" clean "${@}"
