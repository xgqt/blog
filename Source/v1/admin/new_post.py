#!/usr/bin/env python3


"""

""" """

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from os import chdir
from os import path

from subprocess import run
from sys import argv


def main():
    """! Main."""

    script_path = path.realpath(__file__)
    script_root = path.dirname(script_path)

    source_root = path.realpath(path.join(script_root, ".."))

    new_post_name = argv[1]
    leftover_args = argv[2::]
    command_arguments = [
        "sh",
        "./blog-frog/frog.sh",
        "--very-verbose",
        "--new",
        new_post_name,
    ] + leftover_args

    print(f" * Running from: {script_path}")

    print(f" * Entering directory: {source_root}")
    chdir(source_root)

    cmd_string = " ".join(command_arguments)
    print(f" * Executing command: {cmd_string}")

    run(command_arguments, check=True)


if __name__ == "__main__":
    main()
