#!/usr/bin/env python3


"""

""" """

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from os import chdir
from os import path

from subprocess import run


def main():
    """! Main."""

    script_path = path.realpath(__file__)
    script_root = path.dirname(script_path)

    source_root = path.realpath(path.join(script_root, ".."))

    print(f" * Running from: {script_path}")

    print(f" * Entering directory: {source_root}")
    chdir(source_root)

    command_arguments_1 = [
        "sh",
        "./blog-frog/frog.sh",
        "--very-verbose",
        "--build",
    ]

    cmd_string = " ".join(command_arguments_1)
    print(f" * Executing command: {cmd_string}")

    run(command_arguments_1, check=True)

    command_arguments_2 = [
        "sh",
        "./blog-frog/frog.sh",
        "--port",
        "5109",                                    # 1337 -> english == blog :D
        "--very-verbose",
        "--watch",
        "--serve",
    ]

    cmd_string = " ".join(command_arguments_2)
    print(f" * Executing command: {cmd_string}")

    try:
        run(command_arguments_2, check=True)
    except KeyboardInterrupt:
        print(" * Received shutdown signal.")


if __name__ == "__main__":
    main()
