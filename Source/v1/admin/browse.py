#!/usr/bin/env python3


"""

""" """

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


import webbrowser

from os import path

from time import sleep

from urllib.error import URLError
from urllib.request import Request
from urllib.request import urlopen


def main():
    """! Main."""

    script_path = path.realpath(__file__)

    print(f" * Running from: {script_path}")

    target_url = "http://localhost:5109/blog/"

    print(f' * Waiting for webpage "{target_url}"...')

    while True:
        request = Request(target_url)

        try:
            with urlopen(request):
                pass

            break
        except URLError:
            sleep(1)

    webbrowser.open_new_tab(target_url)


if __name__ == "__main__":
    main()
