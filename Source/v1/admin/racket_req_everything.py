#!/usr/bin/env python3


"""

""" """

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""


from os import chdir
from os import path

from subprocess import run


def main():
    """! Main."""

    script_path = path.realpath(__file__)
    script_root = path.dirname(script_path)

    source_root = path.realpath(path.join(script_root, ".."))

    print(f" * Running from: {script_path}")

    print(f" * Entering directory: {source_root}")
    chdir(source_root)

    command_arguments = [
        "racket",
        "-l",
        "raco",
        "req",
        "--verbose",
        "--everything",
    ]

    command_string = " ".join(command_arguments)
    print(f" * Executing command: {command_string}")

    run(command_arguments, check=True)


if __name__ == "__main__":
    main()
