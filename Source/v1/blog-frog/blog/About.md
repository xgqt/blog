# About

I'm a [Gentoo](https://www.gentoo.org/) developer,
[Scheme](https://racket-lang.org/) user and
[Libre](https://opensource.org/osd/) software enjoyer.

In Gentoo I take care of
[ELisp](https://packages.gentoo.org/maintainer/gnu-emacs@gentoo.org),
[Scheme](https://packages.gentoo.org/maintainer/scheme@gentoo.org),
[OCaml](https://packages.gentoo.org/maintainer/ml@gentoo.org),
[DotNET](https://packages.gentoo.org/maintainer/dotnet@gentoo.org),
[Nim](https://packages.gentoo.org/maintainer/nim@gentoo.org) and
[Math](https://packages.gentoo.org/maintainer/sci-mathematics@gentoo.org)
libraries.

Currently writing [Java](https://www.java.com/) and
[Python](https://www.python.org/) for the present
but in the meantime developing
[Racket](https://racket-lang.org/) tools for the future.

He/Him, EN/PL, Europe/Poland

My main website is [xgqt.gitlab.io](https://xgqt.gitlab.io/)
but I also own [xgqt.org](https://xgqt.org/).
So, this blog is also mirrored at
[blog.mirror.xgqt.org](https://blog.mirror.xgqt.org/blog/).

- Socials:
    - Mastodon:
        - [@xgqt@emacs.ch](https://emacs.ch/@xgqt)
          (primary)
        - [@xgqt@fedi.absturztau.be](https://fedi.absturztau.be/xgqt)
          (backup)
        - [@xgqt@fosstodn.org](https://fosstodn.org/@xgqt)
          (inactive)
    - Linkedin:
      [linkedin.com/in/xgqt](https://www.linkedin.com/in/xgqt/)
- Development:
    - OpenHub:
      [openhub.net/accounts/xgqt](https://www.openhub.net/accounts/xgqt/)
    - GitHub:
      [github.com/xgqt](https://github.com/xgqt/)
    - GitLab:
      [gitlab.com/xgqt](https://gitlab.com/xgqt/)
    - Gentoo's GitHub:
      [gitlab.gentoo.org/xgqt](https://gitlab.gentoo.org/xgqt/)
- Donations:
    - PayPal:
      [paypal.me/xgqt](https://paypal.me/xgqt/)

## See also

For some related news check out also the following pages:

- [Gentoo-Racket](https://gentoo-racket.gitlab.io/)
- [Planet Gentoo](https://planet.gentoo.org/)
- [Racket blog](https://blog.racket-lang.org/)

# Copyright

## Posts

Unless stated otherwise the posts on this blog are licensed under
the [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/) license.

## Source

Unless stated otherwise, all source code from this project's source repository
used to produce and distribute this blog is licensed under
the [MPL-2.0](https://opensource.org/license/mpl-2-0/) license.
