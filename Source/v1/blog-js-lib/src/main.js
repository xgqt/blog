// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.


import 'jquery/dist/jquery.slim.min.js';

import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';

import { dom } from "@fortawesome/fontawesome-svg-core";
import { library } from "@fortawesome/fontawesome-svg-core";

import { faTwitter } from "@fortawesome/free-brands-svg-icons/faTwitter";
import { faLinkedin } from "@fortawesome/free-brands-svg-icons/faLinkedin";

import { faAddressCard } from "@fortawesome/free-solid-svg-icons/faAddressCard";
import { faArrowUp } from "@fortawesome/free-solid-svg-icons/faArrowUp";
import { faAtom } from "@fortawesome/free-solid-svg-icons/faAtom";
import { faBlog } from "@fortawesome/free-solid-svg-icons/faBlog";
import { faCodeFork } from "@fortawesome/free-solid-svg-icons/faCodeFork";
import { faEnvelopeSquare } from "@fortawesome/free-solid-svg-icons/faEnvelopeSquare";
import { faRss } from "@fortawesome/free-solid-svg-icons/faRss";
import { faTags } from "@fortawesome/free-solid-svg-icons/faTags";


library.add(
  faLinkedin,
  faTwitter,

  faAddressCard,
  faArrowUp,
  faAtom,
  faBlog,
  faCodeFork,
  faEnvelopeSquare,
  faRss,
  faTags,
);

dom.watch();
