# XGQT's blog

Source repository of XGQT's blog.

## Structure

Current major version is `v2`, see `Source/v2`.

Main content is in `Source/v2/blog-posts`.

## License

### Blog posts

Unless stated otherwise the posts on this blog are licensed under
the [CC-BY–4.0](https://creativecommons.org/licenses/by/4.0/) license.

### Source code

Unless otherwise stated all source code in this repository is licensed under
the [MPL-2.0](https://opensource.org/license/mpl-2-0/) license.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
